var mocha = require('mocha');
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../serverv3');

var should = chai.should()

chai.use(chaiHttp)

describe('Pruebas Colombia ', () => {
  it('conecta api ', (done) => {
    chai.request('http://www.bbva.com')
      .get('/')
      .end((err,res) =>{
         //console.log(res)
        res.should.have.status(200)
        done()
      })
  });
  it('conecta un usuario', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users/1')
      .end((err,res) =>{
         //console.log(res)
        res.should.have.status(200)
        done()
      })
  });
  it('conecta lista usrs ', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
      .end((err,res) =>{
  //       console.log(res)
    //    console.log(res.body)
        res.body.should.be.a('array')

        done()
      })
  });
  it('cantidad de  usrs ', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
      .end((err,res) =>{
  //       console.log(res)
    //    console.log(res.body)
        res.body.length.should.be.gte(1)
        done()
      })
  });
  it('valida el primer elemento ', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
      .end((err,res) =>{
  //       console.log(res)
    //    console.log(res.body[0])
        res.body[0].should.have.property('email')
        res.body[0].should.have.property('pwd')
        done()
      })
  });

  it('por borrar ', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
      .end((err,res) =>{
  //       console.log(res)
    //    console.log(res.body[0])
        res.body[0].should.have.property('email')
        res.body[0].should.have.property('pwd')
        done()
      })
  });
  /*
  it('valida el primer elemento ', (done) => {
    chai.request('http://localhost:3000')
      .post('/colapi/v3/users')
      .send('{"first_name":"Juan","last_name":"suarez"}')
      .end((err,res,body) =>{
  //       console.log(res)
    //    console.log(res.body[0])

        done()
      })
  });

*/
});
