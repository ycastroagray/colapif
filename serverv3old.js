//*** API REST
var express = require('express');
var app = express();
var requestJSON = require('request-json');
var bodyParser = require('body-parser');
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = "/colapi/v3/";

var baseMLabURL ='https://api.mlab.com/api/1/databases/colapidb_fyca/collections/';
var apikeyMLab = 'apiKey=NFP0mvoRwySfUFzNEcMZBBMySivQimYc';
app.listen(port);
console.log("prueba puerto v3...:"+port+"....");
app.use(bodyParser.json());


//petcion GET con mlab
// lista todos los usuarios
app.get(URLbase +'users',
    function (req, res) {
      console.log("GET /colapi/v3/");
      var httpClient =requestJSON.createClient(baseMLabURL);
      console.log("cliente http creado ");
      var queryString = 'f={"_id":0}&' // quita el id que crea mongo
      httpClient.get('user?' + queryString + apikeyMLab,
        function(err,respuestaMlaB,body){
          console.log('error'+ err);
          console.log('R malabr'+ respuestaMlaB);
      //    console.log('body'+ body);
      //    var respuesta = body;
          var respuesta = {};  //para controlar los errores
          respuesta = !err ? body : {"msg":"error al traer mlab"};
          res.send(respuesta);
        //  res.send({"msg": "Respuesta GET OK"});
        });
});

// get al mlab con un solo id

app.get(URLbase +'users/:id',
    function (req, res) {
      console.log("GET /colapi/v3/users/:id");
      console.log(req.params.id);
      var id = req.params.id;
      var queryString = 'q={"id":' + id +'}&';
      var httpClient =requestJSON.createClient(baseMLabURL);
      httpClient.get('user?' + queryString + apikeyMLab,
        function(err,respuestaMlaB,body){
            console.log("respuesta ok");
            var respuesta = {};  //para controlar los errores
            respuesta = !err ? body[0] : {"msg":"error al traer mlab"};
            //respuesta = body[0]; // [0] para traer el json y no el array
            res.send(respuesta);
          });
});

// lista todas las cuentas
app.get(URLbase +'accounts',
    function (req, res) {
      console.log("GET /colapi/v3/");
      var httpClient =requestJSON.createClient(baseMLabURL);
      console.log("cliente http creado ");
      var queryString = 'f={"_id":0}&' // quita el id que crea mongo
      httpClient.get('account?' + queryString + apikeyMLab,
        function(err,respuestaMlaB,body){
          console.log('error'+ err);
          console.log('R malabr'+ respuestaMlaB);
      //    console.log('body'+ body);
      //    var respuesta = body;
          var respuesta = {};  //para controlar los errores
          respuesta = !err ? body : {"msg":"error al traer mlab"};
          res.send(respuesta);
        //  res.send({"msg": "Respuesta GET OK"});
        });
});

// /user/:id/account/:iban/movements
// lista cuentas de un userid
app.get(URLbase +'accounts/:userid',
    function (req, res) {
      console.log("GET /colapi/v3/accounts/userid");
      console.log(req.params.userid);
      var userid = req.params.userid;
      var queryString = 'q={"userid":' + userid +'}&';
      var httpClient =requestJSON.createClient(baseMLabURL);
      httpClient.get('account?' + queryString + apikeyMLab,
        function(err,respuestaMlaB,body){
            console.log("respuesta ok");
            var respuesta = {};  //para controlar los errores
        //    respuesta = !err ? body[0] : {"msg":"error al traer mlab"};
            respuesta = !err ? body : {"msg":"error al traer mlab"};
            res.send(respuesta);
          });
});




//lista todos los movimientos

app.get(URLbase +'movements',
    function (req, res) {
      console.log("GET /colapi/v3/");
      var httpClient =requestJSON.createClient(baseMLabURL);
      console.log("cliente http creado ");
      var queryString = 'f={"_id":0}&' // quita el id que crea mongo
      httpClient.get('movement?' + queryString + apikeyMLab,
        function(err,respuestaMlaB,body){
          console.log('error'+ err);
          console.log('R malabr'+ respuestaMlaB);
      //    console.log('body'+ body);
      //    var respuesta = body;
          var respuesta = {};  //para controlar los errores
          respuesta = !err ? body : {"msg":"error al traer mlab"};
          res.send(respuesta);
        //  res.send({"msg": "Respuesta GET OK"});
        });
});

// falta implementar con lenght el nuevo user
app.post(URLbase +'users',
function(req, res) {
  console.log(">>POST");
 var clienteMlab = requestJSON.createClient(baseMLabURL + "/user?" + apikeyMLab);
 console.log(clienteMlab);
 clienteMlab.post('', req.body,
 function(err, resM, body) {
   console.log("error" + err);
   console.log("resM" + resM);
   res.send(body);
 });
});


app.put(URLbase +'users/:id',
function(req, res) {
    console.log(">>PUT");
    console.log("GET /colapi/v3/users/:id");
    console.log(req.params.id);

 var clienteMlab = requestJSON.createClient(baseMLabURL  + "/user?");
 var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
 clienteMlab.put('?q={"id": ' + req.params.id + '}&' + apikeyMLab, JSON.parse(cambio),
  function(err, resM, body) {
    console.log("error" + err);
    console.log("resM" + resM);
    console.log(req.body);

    res.send(body);
  });
});


//..
// login en construccion
app.post(URLbase +'logines',
  function(req, res) {
    var id = (req.body.id);
    var correo = (req.body.email);
    var clave = (req.body.pwd);

    var queryString = 'q={"id":' + id +'}&';
    //var queryString = 'q={"email":' +correo+'}&';
    var httpClient =requestJSON.createClient(baseMLabURL);
    httpClient.get('user?' + queryString + apikeyMLab,
        function(err,respuestaMlaB,body){
            console.log(body);

        });
  });

// body.length == 0 no existe "respuesta de mlab"
/*
app.post('/v3/usuarios', function(req, res) {
 clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
 clienteMlab.post('', req.body, function(err, resM, body) {
   res.send(body)
 })
})

app.put('/v3/usuarios/:id', function(req, res) {
 clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios")
 var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
 clienteMlab.put('?q={"idusuario": ' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
   res.send(body)
 })
})

*/
