# imagen inicial a partir de la cual creamos nuestra imagen
FROM node
#definimos el directorio del contenedor
WORKDIR /colapi_fyca
#Añadimos contenido dle proyecto en directorio de contenedor
ADD .  /colapi_fyca
#puerto por donde escucha el contenedor
EXPOSE 3000
# comandos para lanzar nuestra api rest 'colapi'
CMD ["npm","start"]
# CMD ["npm" "server3.js"]
